<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>

	<div class="sidebar-wrapper">
        <?php
            dynamic_sidebar( 'sidebar' );
        ?>
	</div>

<?php endif; ?>