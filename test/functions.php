<?php
    /**
     * Класс настройка темы
     */
    class Theme {

        var $themepath;
        var $themeurl;
        var $themename = 'test';

        public function __construct() {

            date_default_timezone_set('Asia/Irkutsk');

            $this->themepath = get_stylesheet_directory();
            $this->themeurl = get_stylesheet_directory_uri();

            if(!is_admin() && !is_network_admin()){
                add_action('wp_enqueue_scripts', array($this, 'addJSCSS'));
            }

            add_action('after_setup_theme', array($this,'addThemeSupport'));
            add_action('init', array($this, 'createNewsPostType'));
            add_action('init', array($this, 'registerMenus'));
            add_action('widgets_init', array($this, 'registerSidebars'));

            add_action('post_submitbox_misc_actions', array($this, 'addSliderCheckbox'));
            add_action('save_post', array($this, 'saveSliderCheckbox'));
        }

        /**
         * Создание нового типа данных "Новости"
         */
        function createNewsPostType(){
            $labels = array(
                'name'                => 'Новости',
                'singular_name'       => 'Новость',
                'menu_name'           => 'Новости',
                'all_items'           => 'Все новости',
                'view_item'           => 'Просмотреть',
                'add_new_item'        => 'Добавить новую новость',
                'add_new'             => 'Добавить новую',
                'edit_item'           => 'Редактировать новость',
                'update_item'         => 'Обновить новость',
                'search_items'        => 'Найти новость',
                'not_found'           => 'Не найдено',
                'not_found_in_trash'  => 'Не найдено в корзине'
            );
            $args = array(
                'labels'              => $labels,
                'supports'            => array('title', 'thumbnail'),
                'taxonomies'          => array(), // категории, которые мы создадим ниже
                'public'              => true,
                'menu_position'       => 6,
                'menu_icon'           => 'dashicons-book',
            );
            register_post_type( 'news', $args );

        }

        /**
         * Подключение css и js темы
         */
        function addJSCSS(){
            wp_enqueue_style('styles', '/wp-content/themes/'.$this->themename.'/css-compiled/main.css?d='.date('YmdHis'));
            //wp_enqueue_script('script', '/wp-content/themes/'.$this->themename.'/js/script.js?d='.date('YmdHis'));
        }
        /**
         * Включение thumbnails
         */
        function addThemeSupport(){
            add_theme_support( 'post-thumbnails' );
        }
        /**
         * Включение меню
         */
        function registerMenus(){
            register_nav_menus(array(
                'main' => 'Главное меню',
            ));
        }
        /**
         * Регистрация sidebar
         */
        function registerSidebars() {
            register_sidebar(
                array(
                    'id' => 'sidebar', // уникальный id
                    'name' => 'Боковая колонка', // название сайдбара
                    'description' => 'Перетащите сюда виджеты, чтобы добавить их в боковую колонку.', // описание

                    'before_widget' => '<div id="%1$s" class="side widget %2$s">', // по умолчанию виджеты выводятся <li>-списком
                    'after_widget' => '</div></div>',

                    'before_title' => '<h2 class="widget-title">', // по умолчанию заголовки виджетов в <h2>
                    'after_title' => '</h2><div class="widget-body">'
                )
            );
            register_sidebar(
                array(
                    'id' => 'inmenu', // уникальный id
                    'name' => 'Рядом с меню', // название сайдбара
                    'description' => 'Перетащите сюда виджеты, чтобы добавить их рядом с меню.', // описание

                    'before_widget' => '<div class="col %2$s">',
                    'after_widget' => '</div>',

                    'before_title' => '<div class="title">',
                    'after_title' => '</div>'
                )
            );

        }
        /**
         * Чекбокс попадания в слайдер для записей
         * @return type
         */
        function addSliderCheckbox(){
            $post_id = get_the_ID();
            if (get_post_type($post_id) != 'post') {
                return;
            }
            $value = get_post_meta($post_id, '_toslider', true);
            wp_nonce_field('toslider_'.$post_id, 'toslider');
            ?>
            <div class="misc-pub-section misc-pub-section-last">
                <p><input type="checkbox" value="1" <?php checked($value, true, true); ?> name="_toslider" /> Использовать в слайдере</p>
            </div>
            <?php
        }
        /**
         * Сохранение чекбокса сладера
         * @param type $post_id
         * @return type
         */
        function saveSliderCheckbox($post_id){
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
                return;
            }
            if (!isset($_POST['toslider']) || !wp_verify_nonce($_POST['toslider'], 'toslider_'.$post_id)) {
                return;
            }
            if (!current_user_can('edit_post', $post_id)) {
                return;
            }
            if (isset($_POST['_toslider'])) {
                update_post_meta($post_id, '_toslider', $_POST['_toslider']);
            } else {
                delete_post_meta($post_id, '_toslider');
            }
        }

    }

    $Theme = new Theme();

    /**
     * Демо виджет hello, в зависимости от времени суток меняет фон.
     * Цвета задаются в стилях
     */
    add_shortcode('hello', function(){
        return "<div class='test-shortcode test-shortcode--".date('H')."' >Hello World ! ".date('H:i')."</div>";
    });

    /**
     * Корректировка виджета поиска
     * @param string $form
     * @return string
     */
    function search_form( $form ) {
         $form = '<form role="search" method="get" id="search-form" action="' . home_url( '/' ) . '" >
         <input type="search" value="' . get_search_query() . '" name="s" id="s" placeholder=" ПОИСК " />
         </form>';
         return $form;
    }
    add_filter( 'get_search_form', 'search_form');

    /**
     * Вывод даты в норм.виде
     * @param type $t
     * @return type
     */
    function displayDte($t) {
        $monn = array('','января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря');
        $s = date("d", $t)." ".$monn[date("n", $t)].", ".date("Y", $t);
        return ltrim($s, '0');
    }

    /**
     * Отладочный код удалить в продакшене
     */
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    add_filter('show_admin_bar', '__return_false');


    /**
     * Демо виджет algoritm,
     */
    add_shortcode('algoritm', function(){
        ob_start();
        echo '<pre>';

        $a = $res = $sum = array();
        for ($i = 0; $i < 10; $i++) {
            $a[] = rand(1, 9);
        }
        for ($i = 1; $i <= 3; $i++) {
            $res[] = array();
        }
        echo "\nИсходный массив\n\n";
        print_r($a);

        // -------------------------------------------------------------------
        echo "\nМое решение проблемы\n\n";
        //
        arsort($a);
        $n = 0;
        foreach($a as $val){
            foreach($res as $n => $b){
                $sum[$n] = array_sum($b);
            }
            asort($sum); reset($sum);
            $res[key($sum)][] = $val;
        }
        print_r($res);

        // -------------------------------------------------------------------
        echo "\nРешение проблемы на просторах Internet";
        echo "\nhttps://ziscod.com/partition-problem-na-php/\n\n";
        //
        $big_array = $a;
        $big_group = 3;
        function fun_partition($array, $part) {
            $part = round($part);
            $array_len = count($array);

            if($part <= 0 || $array_len < $part) {
                return $array;
            } else {
                $out = array_fill(0, $part, array());
                rsort($array, SORT_NUMERIC);

                foreach($array as $value) {

                    usort($out, function($a, $b){
                        return array_sum($a) > array_sum($b);
                    });
                    $out[0][] = $value;
                }
                return $out;
            }
        }
        print_r(fun_partition($big_array, $big_group));

        // -------------------------------------------------------------------

        echo '</pre>';
        return ob_get_clean();
    });

