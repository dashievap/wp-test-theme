var
    gulp  =  require('gulp'),
    sass  =  require('gulp-sass');
    del  =  require('del');


gulp.task('clean', function() {
    return del.sync('public/wp-content/themes/test');
});

gulp.task('sass', function() {
    return gulp.src('dev/css-scss/main.scss')
        .pipe(sass())
        .pipe(gulp.dest('public/wp-content/themes/test/css-compiled'));
});

gulp.task('php', function() {
    return gulp.src('dev/**/*.php')
        .pipe(gulp.dest('public/wp-content/themes/test'));
});


gulp.task('copy', function() {
    var copyPHP = gulp.src('dev/**/*.php').pipe(gulp.dest('public/wp-content/themes/test'));
    var copyPNG = gulp.src('dev/**/*.png').pipe(gulp.dest('public/wp-content/themes/test'));
    var copyCSS = gulp.src('dev/**/*.css').pipe(gulp.dest('public/wp-content/themes/test'));
});

gulp.task('watcher', function() {
    gulp.watch('dev/css-scss/*.scss', ['sass']);
    gulp.watch('dev/**/*.php', ['php']);

});



gulp.task('default', ['clean','sass', 'copy', 'watcher']);