<?php get_header(); ?>

<h2>Новости</h2>

<?php
    $args = array('post_type' => 'news');
    query_posts($args);
    if (have_posts() ):
        while (have_posts() ) : the_post();
            get_template_part('tpl/post','list');
        endwhile;

    endif;
    wp_reset_query();
?>


<?php get_footer(); ?>