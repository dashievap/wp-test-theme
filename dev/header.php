<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#"
      xmlns:og="http://ogp.me/ns#" <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">



    <?php wp_head(); ?>

    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />

</head>
<body>
    <header>
        <?php
            get_template_part('tpl/line','top');
            get_template_part('tpl/line','menu');
            get_template_part('tpl/line','slider');
        ?>
    </header>

    <main role="main">
        <div class="container">
            <div class="col">

                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 b-sidebar">
                            <?php get_sidebar(); ?>
                            <p>
                                <?php
                                    echo do_shortcode('[hello]');
                                ?>
                            </p>
                            <p class="tst tst1">Каждая мелочь важна !</p>
                            <p class="tst tst2">Каждая мелочь важна !</p>
                        </div>

                        <div class="col-lg-8 b-content">
                            <div class="main-wr">
