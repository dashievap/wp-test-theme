<div class="item">
    <div class="c1">
        <?php
            $img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'thumbnail');
            $img = $img[0];
        ?>
        <img src="<?php echo $img;?>"/>
    </div>
    <div class="c2">
        <p class="text-secondary"><?php echo displayDte(get_post_time());?></p>
        <p><a href="<?php echo get_the_permalink();?>"><?php the_title();?></p>
    </div>

</div>
