<div class="line-menu">
    <div class="container">
        <div class="row">
            <div class="col-md-auto b-menu">
                <?php wp_nav_menu( array('theme_location' => 'main','container'=>false, 'menu_class'=>'b-menu-elems')); ?>

            </div>

            <?php dynamic_sidebar('inmenu'); ?>

        </div>
    </div>
</div>
