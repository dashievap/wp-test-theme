<div class="page">
    <h2><?php the_title();?></h2>
    <div class="main_content">
        <?php
            the_content();

            if(empty(get_the_content())){
                echo 'Извините, страница находится в наполнении.';
            }

        ?>
    </div>
</div>
