<div class="line-slider">
    <div class="line-slider--bg"></div>

    <div class="container">
        <div class="col b-slider carousel" id="carousel1" data-ride="carousel">

                <div class="container b-slider-wr carousel-inner">
                    <?php
                        $args = array(
                            'posts_per_page' => 3,
                            'post_type' => array('post'),
                            'meta_key' => '_toslider',
                            'meta_value' => '1'
                        );
                        $lastposts = get_posts( $args );
                        $active = 'active';
                        foreach( $lastposts as $post ){
                            setup_postdata($post);
                            if(has_post_thumbnail()){
                                $img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                                $img = $img['0'];
                                ?>
                                <div class="carousel-item <?php echo $active;?>">
                                    <div class="row ">
                                        <div class="col-lg-4 b-slider-txt">
                                            <div><?php the_title()?></div>
                                            <div>
                                                <div class="b-slider-dte"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;<span><?php echo displayDte(get_post_time());?></span></div>
                                                <div class="b-slider-btn"><button class="btn btn-sm">новости</button></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 b-slider-foto" style="background: #999 url(<?php echo $img;?>) no-repeat center center;"></div>
                                    </div>
                                </div>
                                <?php
                                $active = '';
                            }
                        }
                        wp_reset_postdata();

                    ?>


                </div>
                <div class="carousel-controls">
                    <a class="carousel-control-prev" href="#carousel1" role="button" data-slide="prev"><i class="fa fa-caret-left" aria-hidden="true"></i></a>
                    <a class="carousel-control-next" href="#carousel1" role="button" data-slide="next"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                </div>

        </div>
    </div>
</div>



