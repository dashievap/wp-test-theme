<h2>Новости /</h2>

<div class="single">
    <h3><?php the_title();?></h3>
    <div class="main_content">
        <?php
            $i = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium');
            $imgsrc = $i[0];

            if(!empty($imgsrc)){
                echo "<p><img src='{$imgsrc}'></p>";
            }

            the_content();

            if(empty(get_the_content())){
                echo 'Извините, страница находится в наполнении.';
            }

        ?>
    </div>
</div>
