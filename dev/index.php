<?php get_header(); ?>

<?php
    if (have_posts() ):
        while (have_posts() ) : the_post();
            get_template_part('tpl/post','list');
        endwhile;
    endif;
?>

<?php get_footer(); ?>